(ns ui.core
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.dom :as rdom]
            [clojure.string :as string :refer [split-lines]]
            [re-com.core :as re-com :refer [h-box v-box box p label title]]
            [re-com.box :refer [flex-child-style]]
            [tools.dice-roller :as dice-roller]
            [tools.aspect-parser :as aspect-parser]))

(def join-lines (partial string/join "\n"))

(enable-console-print!)
(aspect-parser/load-aspect-dir aspect-parser/ASPECTSDIR)

(defonce state        (atom {:last-roll {}
                                        ; {:modifier -1
                                        ;  :rolls    [1 2]
                                        ;  :roll-command "2d6 -1"}
                             :last-roll-command nil}))

(defn roll-dice []
  (println "rolling dice")
  (when-not (empty? (:last-roll-command @state))
    (println (dice-roller/roll-dice-command (:last-roll-command @state)))
    (let [res (dice-roller/roll-dice-command (string/trim (:last-roll-command @state)))]
      (if res (swap! state assoc :last-roll res)))
    (swap! state assoc :last-roll-command nil)))

(defn make-roll-box [rolls]
  (let [children (into [] (for [r rolls]
                            [box
                             :child r
                             :style flex-child-style]))]
    [h-box
     :children children
     :height "60px"
     :justify :around]))

(defn dice-results-component []
  [:div.results
   (if (empty? (:last-roll @state))
     [label :label "No last roll"]
     (let [roll-map (:last-roll @state)
           total (->> (:rolls roll-map)
                      (reduce +)
                      (+ (:modifier roll-map)))
           modifier (:modifier roll-map)
           dice (into [] (for [r (:rolls roll-map)] [:div r]))]
       [:div
        [p "Rolled " total]
        [h-box
         :children dice
         :height "60px"
         :justify :around]
        [p "Modifier: " modifier]]))])

(defn dice-roller-component []
  [v-box :children [[box :align :start
                     :child [title :label "Dice Roller"
                             :level :level2]]
                    [re-com/input-text
                     :model (:last-roll-command @state)
                     :placeholder "type in dice roll"
                     :on-change (fn [x]
                                  (swap! state assoc :last-roll-command x)
                                  (roll-dice))
                     :change-on-blur? true]
                    [dice-results-component]]])

(defn- roll-box-component
  [{:keys [:rolls :modifier :roll-command]
    :as args}]
  (when (:rolls args)
    (fn [args]
      (let [di-boxes (for [roll (:rolls args)]
                       [box :child [p roll]])
            roll-sum (reduce + (:rolls args))]
        [v-box :children [[title
                           :label (:roll-command args)
                           :level :level3]
                          [h-box :children (into [] di-boxes)]]]))))

(defn- make-roll-title
  [])

(defn feature-span
  "Span element displaying rolled feature, with roll details and re-roll options in modal."
  [{:keys [:feature :roll :aspect]
    :as args}]
  (let [show? (atom false)]
    (println "outside inner function feature-span")
    (fn [args]
      (let [feature (atom args)]
        (println "in feature span")
        (println @feature)
        [v-box :children [[:span
                           {:on-click #(reset! show? true)}
                           (:feature @feature)]
                          (when @show?
                            [re-com/modal-panel
                             :backdrop-on-click #(reset! show? false)
                             :child [v-box
                                     :width "300px"
                                     :children [[title
                                                 :level :level4
                                                 :underline? true
                                                 :label (get-in @feature [:roll 0 :roll-command])]
                                                [re-com/gap :size "30px"]
                                                [h-box :children [(for [roll (get-in @feature [:roll 0 :rolls])]
                                                                    [box :child [p roll]])]]
                                                ;; this will not work until state management is ironed out;
                                                ;; the feature being passed into the component is not updated on refresh
                                                ;; [re-com/button
                                                ;;  :label "Refresh"
                                                ;;  :on-click (fn [] (let [aspect (aspect-parser/get-aspect (:aspect @feature))
                                                ;;                        new-aspect ((:selector aspect))]
                                                ;;                    (do
                                                ;;                      (println "aspect" aspect)
                                                ;;                      (println "new aspect" new-aspect)
                                                ;;                      (reset! feature new-aspect)
                                                ;;                      (println "feature" @feature))))]
                                                ]]])]]))))

(defn- feature-container
  "Container for rendering rolled features."
  [{:keys [:feature :roll :aspect] :as args}]
  (when (:feature args)
    (fn [args]

      (println "feature container" args)
      [v-box :children [[feature-span args]]])))

(defn aspects-component []
  (let [aspects-state (atom {})]
    (fn []
      (println "aspects-component" @aspects-state)
      [v-box :align :center
       :children [[re-com/title
                   :label "Aspects"
                   :level :level2]
                  [re-com/input-text
                   :placeholder "which aspect"
                   :change-on-blur? true
                   :model ""
                   :on-change (fn [x]
                                (swap! aspects-state assoc :active-aspect
                                       (aspect-parser/get-aspect x))
                                (swap! aspects-state assoc :last-aspect
                                       ((get-in @aspects-state [:active-aspect :selector]))))]
                  [feature-container (get-in @aspects-state [:last-aspect])]]])))

(defn root-component []
  [v-box
   :align :center
   :children [[box
               :child [re-com/title
                       :label "History Generator"
                       :level :level1]]
              [box
               :child [dice-roller-component]]
              [box
               :child [aspects-component]]]])

(rdom/render
 [root-component]
 (js/document.getElementById "app-container"))
