(ns tools.aspect-parser
  (:require [clojure.string :as string]
            [clojure.edn :as edn]
            [clojure.set]
            [cljs.reader :as reader]
            [tools.dice-roller :as roller]))

(def fs (js/require "fs"))
(def js-path (js/require "path"))

; aspects state
(defonce aspects (atom {}))
(def ASPECTSDIR (.join js-path "src" "aspects"))

(defn list-dir [path]
  (->>
   (.readdirSync fs path)
   (js->clj)))

(defn join-dir [path arg]
  (.join js-path path arg))

(defn get-aspect [name]
  ((keyword name) @aspects))

(defn list-aspects []
  (map #(first %) (seq @aspects)))

(defn load-aspect-dir [dir]
  (loop [current-directory dir
         visited #{}
         to-visit []]
    (println current-directory)
    (println visited)
    (println to-visit)
    (let [files-in-dir (map #(join-dir current-directory %) (list-dir current-directory))
          edn-files (filter #(string/ends-with? % ".edn") files-in-dir)
          dirs (filter #(->> % (.statSync fs) .isDirectory) files-in-dir)
          eligible (filter #((complement contains?) visited %) dirs)
          to-visit (into to-visit eligible)
          visited (conj visited current-directory)]
      ; load all edn files in dir
      (into [] (map #(load-aspect %) edn-files))
      (println "edn-files: " edn-files)
      (println "eligibile: " eligible)
      (println "to-visit: " to-visit)

      (if (empty? to-visit) nil
          (recur (first to-visit)
                 visited
                 (set (rest to-visit)))))))

(defn read-edn [path f]
  (->
   (.readFileSync fs path "utf8")
   f))

(defn process [coll]
  (parse-aspect coll))

(defn load-aspect [fp]
  (let [aspect (-> (read-edn fp process))
        aspect-name (:name aspect)]
    (when aspect-name
      (println (str "Loading" aspect-name))
      (swap! aspects assoc (keyword aspect-name) aspect))))

(def sample "
{ ; This is a sample ruleset for aspect generation.
  ; It's in EDN format, which should be easy enough to edit by hand.
  ; TL;DR: Lines, like this one, that start with ; are comments and are ignored.
  ; The data itself is a map

  ; Name the aspect with a :name keyword:
  :name \"sample\"

  ; Declare dice rules for an aspect with a dice keyword:
  :dice \"1d12 + 1d8\"

  ; Declare possible features using a map from total to the feature.
  ; Keys can be ints or ranges of ints
  :features {
     1        \"This is a statistically impossible feature.\"
     2        \"This is a statistically improbable feature.\"
     3        \"This is a statistically improbable feature.\"
     4        \"This is a statistically improbable feature.\"
     5        \"This is a statistically improbable feature.\"
     6        \"This is a statistically improbable feature.\"
     7        \"This is a statistically improbable feature.\"
     \"8-14\" \"This is a statistically probable feature.\"
     15       \"This is a statistically improbable feature.\"
     16       \"This is a statistically improbable feature.\"
     17       \"This is a statistically improbable feature.\"
     18       \"This is a statistically improbable feature.\"
     19       \"This is a statistically improbable feature.\"
     \"20\"   \"This is a statistically improbable feature.\"
  }
}
")

(defn make-feature-key-vals [[k v]]
  (cond (int? k) {k v}
        (re-find #"-" k)
        (let [[start end] (map js/Number (string/split k #"-"))]
          (into {} (for [n (range start (inc end))] [n v])))
        :else {(js/Number k) v}))

(defn- select-feature [a]
  (let [roll ((:roller a))]
    (->> roll  ; roll dice combination
         (map :rolls)
         flatten
         (apply +)
         ((fn [x] (get-in a [:features x])))
         ((fn [x] {:feature x
                   :roll roll
                   :aspect (:name a)})))))

(defn parse-aspect [edn-str]
  (->> edn-str
       edn/read-string

       ; make feature map easily used
       ((fn [x] (assoc x
                       :features
                       (into {} (map #(make-feature-key-vals %) (:features x))))))

       ; attach roller to aspect
       ((fn [x] (assoc x
                       :roller
                       (roller/make-roller (:dice x)))))

       ; attach selector to aspect
       ((fn [x] (assoc x
                       :selector
                       (fn [] (select-feature x)))))))

(do
  (let [sample-aspect (parse-aspect sample)]
    (assert (not (nil? (:features sample-aspect))))
    (assert (not (nil? (:selector sample-aspect))))
    (assert (some? (:feature ((:selector sample-aspect)))))))
