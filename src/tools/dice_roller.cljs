(ns tools.dice-roller
  (:require [clojure.string :as string]))
(enable-console-print!)

(def DICEPATTERN #"(\d+)?(?:\s*)?d(\d+)(?:\s*)?([+-]\d+)?")

(defn roll-di [n]
  (->> (.random js/Math)
       (* n)
       (.floor js/Math)
       inc))

(defn do-roll [[match & groups]]
  (let [[mult di mod] (map (fn [x] (js/Number x)) groups)
        mult (max mult 1)
        mod (if (js/isNaN mod) 0 mod)]
    (->> (into [] (repeatedly mult (fn [] (roll-di di))))
         ((fn [x] {:rolls x :modifier mod :roll-command match})))))

(defn roll-dice-command [s]
  (some->> s
           string/trim
           (re-matches DICEPATTERN)
           do-roll))

(defn make-roller [s]
  (let [cmds (for [m (re-seq DICEPATTERN s)]
               (first m))
        fns (map (fn [x] (fn [] (roll-dice-command x))) cmds)]
    (fn [] (into [] (for [f fns]
                      (f))))))

(defn roll-4d6-drop-lowest []
  (let [roll (roll-dice-command "4d6")]
    (->> roll
         :rolls
         sort
         reverse
         (take 3)
         (apply +)
         (assoc roll :roll))))

(do
  (let [roll-result (roll-dice-command "\n2d20 +10\n")]
    (assert (= (:modifier roll-result)
               10))
    (assert (= (count (:rolls roll-result))
               2))
    (assert (= (:roll-command roll-result)
               "2d20 +10")))

  (let [roll-result (roll-dice-command "1d6")]
    (assert (= (:modifier roll-result)
               0))
    (assert (= (count (:rolls roll-result))
               1))
    (assert (= (:roll-command roll-result)
               "1d6")))

  (let [roll-result (roll-dice-command "1d6 -2")]
    (assert (= (:modifier roll-result)
               -2))
    (assert (= (count (:rolls roll-result))
               1))
    (assert (= (:roll-command roll-result)
               "1d6 -2")))

  (let [roll-result (roll-dice-command "a bad roll command")]
    (assert (nil? (:roll-command roll-result)))))
